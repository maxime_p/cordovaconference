//document.addEventListener('deviceready', onDeviceReady, false);
var app = ons.bootstrap();
var db;

// localStorage.clear();

var devfest_service = {
    loadData: function() {
        //$.getJSON('https://devfest2015.gdgnantes.com/assets/prog.json', function(data) {
        $.getJSON('data/devfest-2015.json', function(data) { // -> hack BB10 for security reasons
            var strValue = JSON.stringify(data);
            localStorage.setItem("DEV_FEST_PROG", strValue);
        });
    },
    getData: function(callback) {
        var strValue = localStorage.getItem("DEV_FEST_PROG");
        callback(JSON.parse(strValue));
    }
}

devfest_service.loadData();

//list des sessions
$(document).on("pageinit", "#sessions", function(event) {
    devfest_service.getData(function(data) {
        var items = [];
        var sessions = [];

        for (var category in data.categories) {
            items.push("<ons-list-header>" + data.categories[category] + "</ons-list-header>");

            data.sessions.forEach(function(session) {
                if (session.type === category) {
                    var item = "<ons-list-item modifier='chevron' class='overflow-hidden-item' " +
                        "id=\"session_" + session.id + "\">" +
                        session.title +
                        "</ons-list-item>";
                    sessions.push(session);
                    items.push(item);
                }
            });
        }

        $("<ons-list/>", {
            "id": "listSessions",
            html: items.join("")
        }).appendTo("sessions");
        ons.compile(document.getElementById('listSessions'));
        //add all the listeners
        sessions.forEach(function(session) {
            $("#session_" + session.id).on('click', function(event) {
                app.navi.pushPage('views/session_details.html', {
                    session: session
                });
            });
        });
    });
});

//list des speakers
$(document).on("pageinit", "#speakers", function(event) {
    devfest_service.getData(function(data) {
        var items = [];
        var speakers = [];
        var speakersSorted = data.speakers.sort(function(a, b) {
            if (a.firstname < b.firstname) return -1;
            if (a.firstname > b.firstname) return 1;
            return 0;
        });
        speakersSorted.forEach(function(speaker) {
            var about = speaker.about;
            if (about.length > 150)
                about = about.substring(0, 150) + "...";

            var domString =
                "<ons-list-item modifier='chevron' class='list-item-container' " +
                "id=\"speaker_" + speaker.id + "\">" +
                ">" +
                "  <div class='list-item-left'>" +
                "    <img src='data/images/" + speaker.image + "' class='avator'>" +
                "  </div>" +
                "  <div class='list-item-right'>" +
                "    <div class='list-item-content'>" +
                "      <div class='name'>" + speaker.firstname + " " + speaker.lastname +
                "        <span class='lucent'>@ " + speaker.id + "</span>" +
                "      </div>" +
                "      <span class='desc'>" + about + "</span>" +
                "    </div>" +
                "  </div>" +
                "</ons-list-item>";
            speakers.push(speaker);
            items.push(domString);
        });

        $("<ons-list/>", {
            "id": "listSpeakers",
            html: items.join("")
        }).appendTo("speakers");
        ons.compile(document.getElementById("listSpeakers"));
        //add all the listeners
        speakers.forEach(function(speaker) {
            $("#speaker_" + speaker.id).on('click', function(event) {
                app.navi.pushPage('views/speaker_details.html', {
                    speaker: speaker
                });
            });
        });
    });
});

$(document).on("pageinit", "#session", function(event) {

    var session = app.navi.getCurrentPage().options.session;
    var speakers = [];
    session.speakers.forEach(function(speaker) {
        speakers.push("@" + speaker + " ")
    });
    var content = "<h2>" + session.title + "</h2>" +
        "<p> Salle : " + session.confRoom + "</p>" +
        "<h2>" + speakers.join("") + "</h2>" +
        "<button id='buttonNote' class='button button--large'>Prendre des notes</button><br>" +
        "<p>" + session.desc + "</p>";
    $("<div/>", {
        "class": "centerMargin",
        html: content
    }).appendTo("session");
    $("#buttonNote").on('click', function(event) {
        app.navi.pushPage('views/note.html', {
            session: session
        });
    });
    $(document).on('click', '#existingPicture', function(event) {
        getExistingPicture();
    });
    $(document).on('click', '#takePicture', function(event) {
        getPicture();
    });
    $(document).on('click', '#takeSound', function(event) {
        takeAudio();
    });
    $(document).on('click', '#takeVideo', function(event) {
        takeVideo();
    });
    $(document).on('click', '#saveNote', function(event) {
        var media = {
            "notes": "",
            "photos": [],
            "videos": [],
            "audios": []
        };
        media.notes = $('#noteText').val();
        $('#notes_picture_list').children().each(function(index) {
            if ($(this)[0].tagName == "IMG") {
                media.photos.push($(this).prop('src'));
            }
            if ($(this)[0].tagName == "AUDIO") {
                media.audios.push($(this).prop('src'));
            }
            if ($(this)[0].tagName == "VIDEO") {
                media.videos.push($(this).prop('src'));
            }
        });
        localStorage.setItem(session.id, JSON.stringify(media)); //BB10 hack
    });

});

$(document).on('pageinit', '#sessionPageNote', function(event) {

    var session = app.navi.getCurrentPage().options.session;

    $('#sessionNoteTitle').text(session.title);

    // hack BB10
    var media = JSON.parse(localStorage.getItem(session.id));

    if (media != null) {
        $('#noteText').text(media.notes);
        media.photos.forEach(function(source) {
            $("#notes_picture_list").append('<img src="' + source + '" style="width:100%;"/>');
        });
        media.videos.forEach(function(source) {
            $("#notes_picture_list").append('<video width="100%" controls><source src="' + source + '"></video>');
        });
        media.audios.forEach(function(source) {
            $("#notes_picture_list").append('<audio controls><source src="' + source + '"/></audio>');
        });
    }

    // db.executeSql('SELECT * FROM NOTES WHERE sessionId = ?', [session.id], function(res) {
    //       if (res.rows.length > 0) {
    //             $('#noteText').text(res.rows.item(0).comment);
    //       }
    // }); // -> OK sur android
});
/*
function onDeviceReady(){
	db = window.sqlitePlugin.openDatabase({name: "app.conference"});
	db.executeSql('DROP TABLE IF EXISTS NOTES');
    db.executeSql('CREATE TABLE IF NOT EXISTS NOTES (comment text, sessionId text primary key)');
}

function writeBDD(sessionId, note){

	localStorage.setItem("note-"+sessionId, note); //BB10 hack
	db.executeSql("INSERT OR REPLACE INTO NOTES (comment, sessionId) VALUES (?, ?)", [note, sessionId]);
}
*/

function findContact(displayName, callback) {
    function onSuccess(contacts) {
        if (contacts.length > 0) {
            alert("Find Success "+JSON.stringify(contacts[0]));
            callback(contacts[0]);
        } else {
            alert("Pas de contacts trouvÃ©s");
        }
    };

    function onError(contactError) {
        alert("Error = " + contactError.code);
    };
    var monContact = navigator.contacts.create();
    var optionsRecherche = new ContactFindOptions();
    optionsRecherche.filter = displayName;
    optionsRecherche.desiredFields = [navigator.contacts.fieldType.id];
    var champsRecherche = [navigator.contacts.fieldType.displayName]; // --> hack BB10, l'id est autogénéré, pas de recherche dessus
    navigator.contacts.find(champsRecherche, onSuccess, onError, optionsRecherche);
}

function removeContact(displayName) {
    function onSuccess(contact) {
        alert("Remove Success");
    };
    function onError(contactError) {
        alert("Error = " + contactError.code);
    };

    findContact(displayName, function(contact) {
        contact.remove(onSuccess, onError);
    });
}

function createContact(speaker) {
    function onSuccess(contact) {
        alert("Create Success");
    };

    function onError(contactError) {
        alert("Error = " + contactError.code);
    };
    var monContact = navigator.contacts.create();
    var pictures = [];
    var field = new ContactField();
    field.value = speaker.image;
    field.pref = false;
    pictures.push(field);
    monContact.photos = pictures;
    monContact.nickname = speaker.id;
    var name = new ContactName();
    name.givenName = speaker.firstname;
    name.familyName = speaker.lastname;
    monContact.name = name;
    monContact.displayName = speaker.firstname + " " + speaker.lastname;
    //monContact.note = speaker.desc;
    var urls = [];
    speaker.socials.forEach(function(social) {
        var field = new ContactField();
        field.type = social.class;
        field.value = social.link;
        field.pref = false;
        urls.push(field);
    });
    //monContact.urls = urls;
    //monContact.note = speaker.about;
    var organizations = [];
    var organization = new ContactOrganization();
    organization.type = "organization";
    organization.name = speaker.company;
    organization.pref = "false";
    organizations.push(organization);
    //monContact.organizations = organizations;
    monContact.save(onSuccess, onError);
}

$(document).on("pageinit", "#speaker", function(event) {

    var speaker = app.navi.getCurrentPage().options.speaker;
    var socials = [];
    speaker.socials.forEach(function(social) {
        var item = "<a href='" + social.link + "'><span class='fa fa-" + social.class + " social-link'></span></a> ";
        socials.push(item);
    });

    // is the contact already in the contacts list ?
    findContact(speaker.firstname + " " + speaker.lastname, function(contact) {
        alert("Existe dÃ©jÃ ");
        $("#checkContact").prop('checked', true);
    });

    var content =
        "<ons-list>" +
        "<ons-list-item>" +
        "<ons-icon icon='fa-user' size='20px'></ons-icon>" +
        "Ajouter Ã  mes contacts" +
        "<label class='switch switch--list-item'>" +
        "<input id='checkContact' type='checkbox' class='switch__input' >" +
        "<div class='switch__toggle'></div>" +
        "</label>" +
        "</ons-list-item>" +
        "</ons-list>" +
        "<div class='profile-card'>" +
        "<img src='data/images/" + speaker.image + "' class='profile-image'>" +
        "<div class='profile-name'>" + speaker.firstname + " " + speaker.lastname + "</div>" +
        "<div class='profile-id'>@" + speaker.id + "</div>" +
        "<div class='profile-desc'>" + speaker.about + "</div>" +
        socials.join("") +
        "</div>";

    $("<div/>", {
        "id": "details",
        html: content
    }).appendTo("speaker");
    ons.compile(document.getElementById("details"));

    //add listener
    $("#checkContact").change(function() {
        if ($("#checkContact").prop("checked")) {
            createContact(speaker);
        } else {
            removeContact(speaker.firstname + " " + speaker.lastname);
        }
    });
});

$(document).on("pageinit", "#technique", function(event) {

    var states = {};
    states[Connection.UNKNOWN] = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI] = 'WiFi connection';
    states[Connection.CELL_2G] = 'Cell 2G connection';
    states[Connection.CELL_3G] = 'Cell 3G connection';
    states[Connection.CELL_4G] = 'Cell 4G connection';
    states[Connection.CELL] = 'Cell generic connection';
    states[Connection.NONE] = 'No network connection';
    var network = function() {
        return states[navigator.connection.type];
    }

    var items =
        "<li  class='list__item'>" + "<span style='font-weight:bold'> Available : </span>" + device.available + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Platform : </span>" + device.platform + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Version : </span>" + device.version + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Uuid : </span>" + device.uuid + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Cordova : </span>" + device.cordova + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Model : </span>" + device.model + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Manufacturer : </span>" + device.manufacturer + "</li>" +
        "<li class='list__item'>" + "<span style='font-weight:bold'> Connexion : </span>" + network(navigator.connection.type) + "</li>" +
        "</ul>";

    $("<ons-list/>", {
        "id": "listTechnique",
        html: items
    }).appendTo("technique");
    ons.compile(document.getElementById('listTechnique'));
});

function takeAudio() {
    navigator.device.capture.captureAudio(function(mediaFiles) {
        mediaFiles.forEach(function(file) {
            $("#notes_picture_list").append('<audio controls><source src="' + file.fullPath + '"/></audio>');
        });
    });
}

function takeVideo() {
    navigator.device.capture.captureVideo(function(mediaFiles) {
        mediaFiles.forEach(function(file) {
            $("#notes_picture_list").append('<video width="100%" controls><source src="' + file.fullPath + '"></video>');
        });
    });
}

// photos
function getPicture() {
    navigator.camera.getPicture(onSuccess, onFail, {
        destinationType: Camera.DestinationType.FILE_URI,
        targetWidth: 500,
        targetHeight: 500
    });

    function onSuccess(imageURI) {
        $("#notes_picture_list").append('<img src="' + imageURI + '" style="width:100%;"/>');
    }

    function onFail(message) {
        alert('Failed because: ' + message);
    }
}

function getExistingPicture() {
    navigator.camera.getPicture(onSuccess, onFail, {
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
    });

    function onSuccess(imageURI) {
        $("#notes_picture_list").append('<img src="' + "data:image/jpeg;base64," + imageURI + '" style="width:100%;"/>');
    }

    function onFail(message) {
        alert('Failed because: ' + message);
    }
}
